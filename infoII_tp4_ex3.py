#https://gitlab.unige.ch/Nam-Thang.Nguyen/infoII-tp4.git

class File:
    def __init__(self):
        self.items = []

    def afficher(self):
        return self.items
    
    def enqueue(self, item):
        #on ajoute l'élément au début de la queue (fin de la liste)
        self.items.append(item)
        #on fait reculer l'élément ajouté dans la liste tant qu'il a pas la priorité ou qu'il a la même 
        # priorité par rapport à l'élément qui est déjà là
        # (on se déplace vers la gauche) car le début de la queue est tout à droite
        for i in range(len(self.items)-1, 0, -1):
            if(self.items[i][0] >= self.items[i-1][0]):
                temp = self.items[i-1]
                self.items[i-1] = self.items[i]
                self.items[i] = temp
        
    def dequeue(self):
        return self.items.pop()
           

    def is_empty(self):
        return (self.items == [])

    
    

if __name__ == "__main__":
    
    f = File()
    
    etat = True
    
    while(etat==True):      #possibilité pour l'utilisateur de faire un choix
        print("\nPour ajouter une personne, pressez 1 ")
        print("Pour enlever une personne, pressez 2")
        print("Pour arrêter le programme, pressez n'importe quelle touche")

        choix = input("\nchoix: ")
        if(choix=='1'):
            priority = int(input("Entrez l'ordre que vous voulez ajouter: "))
            nom = input("Entrez le nom de la personne: ")
            data = (priority, nom)
            f.enqueue(data)
            print("Voici la queue mise à jour: " + str(f.afficher()))

        elif(choix=='2'):
            #si la queue est vide, alors on peut pas retirer de personnes
            if(f.is_empty()):
                print("Il n'y a personne dans la queue")
            else:
                f.dequeue()
                print("Voici la queue mise à jour: " + str(f.afficher()))

        else:
            etat = False    #possibilité de sortir du programme pour que la boucle ne tourne pas à l'infini"""


