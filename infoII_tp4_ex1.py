class Pile:
    def __init__(self):
        #liste 1 sert de texte
        self.liste1 = []
        
        #la liste 2 nous sert à stocker les valeurs qui ont été supprimées (corbeille)
        self.liste2 = []

    def is_empty1(self):
        #vérifie si le texte est vide
        return (self.liste1 == [])
    
    def is_empty2(self):
        #vérifie si la corbeille est vide
        return (self.liste2 == [])

    def push(self, item):
        #on ajoute l'élément dans le texte
        self.liste1.append(item)
        #on réinitialise la corbeille car on peut plus rétablir avant d'annuler
        self.liste2 = []
    
    def pop(self):
        #quand on annule, on ajoute l'élément qu'on a annulé dans la corbeille
        self.liste2.append(self.liste1[len(self.liste1)-1])
        #on retire le mot du texte
        return self.liste1.pop()
    
    
    def push_back(self):
        #on rajoute l'élément le plus récent de la corbeille dans le texte
        self.liste1.append(self.liste2[len(self.liste2)-1])
        #lorsqu'on rétablit un élément, on l'enlève de la corbeille
        return self.liste2.pop()
    
    
    

class Editeur:

    
    def __init__(self):
        #instance de la pile
        self.pile = Pile()
        
    def ecrire(self, element):
        self.pile.push(element)
        

    def annuler(self):
        #vérifie si le texte est vide car si c'est le cas on peut pas retirer de mots
        if(self.pile.is_empty1()):
            print("Il n'y a aucun élément dans le texte")
        else:    
            #on retire le mot du texte
            self.pile.pop()
            
            
    def retablir(self):
        #vérifie si la corbeille est vide car si c'est le cas on peut pas rétablir le mot
        if(self.pile.is_empty2()):
            print("Pas possible de rétablir")

        else:
            self.pile.push_back()
            

    def afficher(self):
        for i in range(len(self.pile.liste1)):
            print(self.pile.liste1[i], end=" ")
        print("")


if __name__ == "__main__":
    
    e = Editeur()

   
    etat = True
    
    while(etat==True):      #possibilité pour l'utilisateur de faire un choix
        print("\nPour ajouter un élément, pressez 1 ")
        print("Pour annuler l’ajout du dernier élément écrit, pressez 2")
        print("Pour rétablir le dernier élément annulé, pressez 3")
        print("Pour afficher le texte entier sous forme d’une chaîne de caractères, pressez 4")
        print("Pour arrêter le programme, pressez n'importe quelle touche")

        choix = input("\nchoix: ")
        if(choix=='1'):
            element = input("Entrez l'élément que vous voulez ajouter: ")
            e.ecrire(element)
        elif(choix=='2'):
            e.annuler()
        elif(choix=='3'):
            e.retablir()
        elif(choix=='4'):
            e.afficher()
        else:
            etat = False    #possibilité de sortir du programme pour que la boucle ne tourne pas à l'infini   
