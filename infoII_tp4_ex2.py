from collections import deque


class Editeur:

    
    def __init__(self):
        #instances
        self.pile1 = deque()
        self.pile2 = deque()
        self.retabli = False
        
    def ecrire(self, element):
        self.pile1.append(element)
        self.pile2 = deque()

    def annuler(self):
        #vérifie si le texte est vide car si c'est le cas on peut pas retirer de mots
        if(len(self.pile1) == 0):
            print("Il n'y a aucun élément dans le texte")
        else:    
            #sinon, on ajoute l'élément à la corbeille
            self.pile2.append(self.pile1[len(self.pile1)-1])
            
            #on retire le mot du texte
            self.pile1.pop()

            
    def retablir(self):
        #si la corbeille est vide, on peut pas rétablir d'éléments
        if(len(self.pile2) == 0):
            print("Pas possible de rétablir")

        else:
            #sinon on rajoute l'élément le plus récent de la corbeille dans le texte
            self.pile1.append(self.pile2[len(self.pile2)-1])
            #lorsqu'on rétablit un élément, on l'enlève de la corbeille
            self.pile2.pop()

    
    def afficher(self):
        for i in range(len(self.pile1)):
            print(self.pile1[i], end=" ")


if __name__ == "__main__":
    
    e = Editeur()

   
    etat = True
    
    while(etat==True):      #possibilité pour l'utilisateur de faire un choix
        print("\nPour ajouter un élément, pressez 1 ")
        print("Pour annuler l’ajout du dernier élément écrit, pressez 2")
        print("Pour rétablir le dernier élément annulé, pressez 3")
        print("Pour afficher le texte entier sous forme d’une chaîne de caractères, pressez 4")
        print("Pour arrêter le programme, pressez n'importe quelle touche")

        choix = input("\nchoix: ")
        if(choix=='1'):
            element = input("Entrez l'élément que vous voulez ajouter: ")
            e.ecrire(element)
        elif(choix=='2'):
            e.annuler()
        elif(choix=='3'):
            e.retablir()
        elif(choix=='4'):
            e.afficher()
        else:
            etat = False    #possibilité de sortir du programme pour que la boucle ne tourne pas à l'infini 


